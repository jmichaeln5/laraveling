@extends('layouts/app')
@section('content')

<div class="jumbotron text-center">
    <h1> {{$title}} </h1>
    <p>Welcome to my Laravel App</p>
    <p> 
        <a class="btn btn-primary btn-lrg" href="/login" role="button">Login</a>
        <a class="btn btn-success btn-lrg" href="/register" role="button">Register</a>
    </p>
</div>
@endsection