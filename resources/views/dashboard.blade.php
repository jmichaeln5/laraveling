@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
 

                    <h3 class="text-center"> Your Blog Posts </h3>
                    <hr>
                    <br>
                        <a href="/posts/create"class="text-center btn btn-primary btn-block">Create Post</a>
                    <br>
                    <br>
        
                    @if(count($posts) > 0 )
                    <table class="table table-striped">
                        <tr>
                            {{-- <th></th> --}}
                        </tr>
                        
                        @foreach($posts as $post)
                        <tr>
                            <td><a href="/posts/{{$post->id}}"> {{$post->title}}</a></h3> </td>
                            <td><a href="/posts/{{$post->id}}" class="btn btn-secondary"> Show</a></td>
                            <td><a href="/posts/{{$post->id}}/edit" class="btn">Edit</a> </td>
                            <td>
                                {!! Form::open(['action'=> ['PostsController@destroy', $post->id], 'method' => 'POST', 'class'=> 'pull-right'] )!!}
                                {{Form::hidden('_method', 'DELETE')}}
                                {{Form::submit('Delete', ['class'=> 'btn btn-danger'])}}
                                {!! Form::close() !!}
                            </td>
                        </tr> 
                        @endforeach
                    </table>
                    @else
                        <p>You have no posts.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
