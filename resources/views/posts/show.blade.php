@extends('layouts/app')

@section('content')

         <div class="well">
         <h3>  {{$post->title}}</h3>
        <div>
            {!!$post->body!!}
        </div>
        <hr>
        <small>Written on {{$post->created_at}} by {{$post->user->name}}</small>
        <br>
        <br>
        <div>
        @if (!Auth::guest())
            @if(Auth::user()->id == $post->user_id)


            <a href="/posts/{{$post->id}}/edit"  type="button" class="btn btn-secondary pull-left">Edit</a>

            <br>
            <br>

            {!! Form::open(['action'=> ['PostsController@destroy', $post->id], 'method' => 'POST', 'class'=> 'pull-right'] )!!}
                {{Form::hidden('_method', 'DELETE')}}
                {{Form::submit('Delete', ['class'=> 'btn btn-danger'])}}
            {!! Form::close() !!}

            @endif
        @endif
    </div>
    <br>
    <hr>
    <br>
            <a href="/posts" type="button" class="btn btn-primary btn-lg btn-block">Back</a>

        
@endsection